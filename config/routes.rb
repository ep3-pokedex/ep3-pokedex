Rails.application.routes.draw do
  devise_for :users
	root 'welcome#homepage'
  	get 'welcome/homepage'
  	get 'devise/passwords/new'
  	get 'devise/passwords/edit'
  	get 'devise/registrarions/new'
  	get 'devise/registrarions/edit'
  	get 'devise/sessions/new'
  	get 'devise/sessions/edit'

  	#root 'welcome#login'
  	#get 'welcome/login'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
